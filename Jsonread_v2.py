from __future__ import print_function
from collections import Counter
from botocore.vendored import requests
import json

def lambda_handler(event, context):
    message = json.loads(event['Records'][0]['Sns']['Message'])
    url = message['url']
    aws_url = requests.get(url)
    json_url = json.loads(aws_url.text)
    return_service = filter_service (json_url)
    return return_service

def filter_service(json_url):
    region_dict = {
    
    }

    for aws_region in json_url['prefixes']:
        region = aws_region['region']
        if aws_region['region'] not in region_dict:
            region_dict[aws_region['region']] = [aws_region['service']]
        else:
            region_dict[aws_region['region']].append(aws_region['service'])

    for (key, value) in region_dict.items():
        print (key, Counter(list(filter(None, value))))
