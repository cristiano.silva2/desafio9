Documentação

**Criar Bucket S3**

``````
https://s3.console.aws.amazon.com/s3/buckets/crisdesafio9?region=us-east-1&tab=objects
``````

**Após criar o Bucket, carregar arquivo "Jsonread.zip"**

``````
https://gitlab.com/mandic-labs/aceleracao-mandic/aceleracao-sre/desafios/-/tree/master/desafio-09
``````

**Após carregar o arquivo, editar o arn:aws:sns e https://crisdesafio9.s3.amazonaws.com/Jsonread.zip**

``````
{
    "Records": [
        {
            "EventVersion": "1.0",
            "EventSubscriptionArn": "arn:aws:sns:EXAMPLE",
            "EventSource": "aws:sns",
            "Sns": {
                "SignatureVersion": "1",
                "Timestamp": "1970-01-01T00:00:00.000Z",
                "Signature": "EXAMPLE",
                "SigningCertUrl": "EXAMPLE",
                "MessageId": "95df01b4-ee98-5cb9-9903-4c221d41eb5e",
                "Message": "{\"create-time\": \"yyyy-mm-ddThh:mm:ss+00:00\", \"synctoken\": \"0123456789\", \"md5\": \"532fd4e00d3c51e8459490f49274be8c\", \"url\": \"https://crisdesafio9.s3.amazonaws.com/Jsonread.zip\"}",
                "Type": "Notification",
                "UnsubscribeUrl": "EXAMPLE",
                "TopicArn": "arn:aws:sns:EXAMPLE",
                "Subject": "TestInvoke"
                }
            }
        ]
     }

``````

**Feito a alteração, Criar o arquivo Lambda.tf;**

``````

resource "aws_lambda_function" "jsonread_func" {

  s3_bucket     = "crisdesafio9"
  s3_key        = "Jsonread.zip"

  function_name = "lambda_def09_cris"
  role          = aws_iam_role.lambda_iam_role.arn
  handler       = "Jsonread.handler"
  runtime       = "python3.6"

  tags = {
        "name" = "cristiano",
        "team" = "pulmao"
    }
}

resource "aws_lambda_permission" "with_sns" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.jsonread_func.function_name
  principal     = "sns.amazonaws.com"
  source_arn    = "arn:aws:sns:us-east-1:806199016981:AmazonIpSpaceChanged"
}

resource "aws_iam_role" "lambda_iam_role" {
  name = "def09_cris"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_cloudwatch_log_group" "cw_group" {
  name              = "/aws/lambda/${aws_lambda_function.jsonread_func.function_name}"
  retention_in_days = 14
}

resource "aws_iam_policy" "lambda_logging" {
  name        = "lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.lambda_iam_role.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}

``````

**Criar arquivo,  Provider.tf para expecificar qual é**

``````
provider "aws" {
    region = var.aws_region
}
``````
**Criar arquivo variables.tf**

``````
variable "aws_region"{
    default = "us-east-1"
}
``````

**Criar arquivo em Python;**

``````
from __future__ import print_function
from collections import Counter
from botocore.vendored import requests
import json

def lambda_handler(event, context):
    message = json.loads(event['Records'][0]['Sns']['Message'])
    url = message['url']
    aws_url = requests.get(url)
    json_url = json.loads(aws_url.text)
    return_service = filter_service (json_url)
    return return_service

def filter_service(json_url):
    region_dict = {
    
    }

    for aws_region in json_url['prefixes']:
        region = aws_region['region']
        if aws_region['region'] not in region_dict:
            region_dict[aws_region['region']] = [aws_region['service']]
        else:
            region_dict[aws_region['region']].append(aws_region['service'])

    for (key, value) in region_dict.items():
        print (key, Counter(list(filter(None, value))))
``````



